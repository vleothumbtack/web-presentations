FROM node

COPY . /opt/app

WORKDIR /opt/app

RUN npm install
RUN npm run build