const pathTo = (dirName) => require('path').resolve(__dirname, dirName);
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');

if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = 'development'
}

const devMode = process.env.NODE_ENV !== 'production';

module.exports = {
  context: pathTo('src'),
  entry: [
    './index.js',
    './index.scss'
  ],
  output: {
    path: pathTo('public/assets'),
    filename: 'js/[name].[hash].js',
    publicPath: '/assets/'
  },
  devServer: {
    contentBase: pathTo('public'),
    port: 3000
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: pathTo('public/assets'),
            }
          },
          'css-loader',
          'resolve-url-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.(png|jpg|svg|ttf|eot|woff|woff2|gif)$/,
        use: 'url-loader?limit=8192'
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: devMode ? 'css/[name].css' : 'css/[name].[hash].css',
      chunkFilename: devMode ? 'css/[id].css' : 'css/[id].[hash].css',
    }),
    new HtmlWebpackPlugin({
      filename: pathTo('public/index.html'),
      template: 'templates/index.html',
      templateParameters: {
        googleClientId: '1003472031435-b3d56kfccak3sb6tefgmktds9d7a8uf0.apps.googleusercontent.com',
      }
    }),
  ],
  mode: process.env.NODE_ENV
};