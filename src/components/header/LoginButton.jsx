import React, { Component } from 'react';

export default class LoginButton extends Component {
  LOGIN_BUTTON_ID = "google-sign-in-btn";

  componentDidMount() {
    window.gapi.signin2.render(
      this.LOGIN_BUTTON_ID,
      {
        onsuccess: this.onSignIn.bind(this)
      }
    );
  }

  onSignIn(googleUser) {
    const {
      authStateHandler
    } = this.props;

    const profile = googleUser.getBasicProfile();
    const authResponse = googleUser.getAuthResponse({
      includeAuthorizationData: true
    });
    const userEntity = {};

    userEntity.token = authResponse.access_token;
    userEntity.name = profile.getName();
    userEntity.photo = profile.getImageUrl();

    sessionStorage.setItem('userEntity', JSON.stringify(userEntity));

    authStateHandler(true);
  }

  render() {
    return (
      <div id={this.LOGIN_BUTTON_ID} />
    )
  }
}