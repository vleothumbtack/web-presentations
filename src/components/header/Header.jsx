import React, { Component } from 'react';

import {
  AppBar,
  Toolbar
} from '@material-ui/core';

import LoginButton from './LoginButton';
import UserAvatarButton from './UserAvatarButton';

export default class Header extends Component {
  getProfileButton() {
    const {
      authStateHandler
    } = this.props;

    const profile = sessionStorage.getItem('userEntity');

    if (profile) {
      const profileObj = JSON.parse(profile);

      return <UserAvatarButton userName={profileObj.name}
                               userPhotoUrl={profileObj.photo}
                               authStateHandler={authStateHandler} />;
    }

    return <LoginButton authStateHandler={authStateHandler}/>;
  }

  render() {
    return (
      <AppBar className="top-bar" position="relative">
        <Toolbar>
          {this.getProfileButton()}
        </Toolbar>
      </AppBar>
    )
  }
}