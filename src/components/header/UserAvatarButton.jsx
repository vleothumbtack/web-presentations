import React, { Component } from 'react';

import { Avatar } from "@material-ui/core";

export default class UserAvatarButton extends Component {
  signOut() {
    const {
      authStateHandler
    } = this.props;

    const auth2 = window.gapi.auth2.getAuthInstance();

    auth2.signOut().then(() => {
      sessionStorage.removeItem('userEntity');

      authStateHandler(false);
    });
  }

  render() {
    const {
      userName,
      userPhotoUrl
    } = this.props;

    return (
      <Avatar src={userPhotoUrl}
              title={userName}
              onClick={this.signOut.bind(this)} />
    )
  }
}