import React, { Component } from 'react';
import {
  Editor
} from '@tinymce/tinymce-react';

export default class SlideTitle extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: 'Title'
    };
  }

  handleEditorChange(event) {
    this.setState({
      title: event.target.getContent()
    });
  }

  render() {
    const { title } = this.state;

    return (
        <div className="slide__title">
          <Editor
            apiKey='3kpqhhz22n7kh2p2k0ma0vc2au32iux205i3lhoayylqjml3'
            inline={true}
            initialValue={title}
            init={{
              menubar: false,
              toolbar:
              // eslint-disable-next-line no-multi-str
                'undo redo | fontselect | bold italic | \
                alignleft aligncenter alignright alignjustify | \
                bullist numlist outdent indent | removeformat | help'
            }}
            onChange={this.handleEditorChange.bind(this)}
          />
        </div>
    );
  };
}