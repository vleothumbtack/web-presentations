import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Slide from './Slide';

export default class SlideList extends Component {
  static propTypes = {
    slidesList: PropTypes.arrayOf(PropTypes.object).isRequired
  };

  render() {
    return (
      <div className="slide-list">
        <Slide />
        <Slide />
        <Slide />
      </div>
    )
  }
}