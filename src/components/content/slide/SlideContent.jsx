import React, { Component } from 'react';
import {
  Editor
} from '@tinymce/tinymce-react';

export default class SlideContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: 'Content'
    };
  }

  handleEditorChange(event) {
    this.setState({
      content: event.target.getContent()
    });
  }

  render() {
    const { content } = this.state;

    return (
      <div className="slide__content">
        <Editor
          apiKey='3kpqhhz22n7kh2p2k0ma0vc2au32iux205i3lhoayylqjml3'
          inline={true}
          initialValue={content}
          init={{
            menubar: false,
            toolbar:
            // eslint-disable-next-line no-multi-str
              'undo redo | fontselect | bold italic | \
              alignleft aligncenter alignright alignjustify | \
              bullist numlist outdent indent | removeformat | help'
          }}
          onChange={this.handleEditorChange.bind(this)}
        />
      </div>
    );
  };
}