import React, { Component } from 'react';

import {
  Paper
} from "@material-ui/core";

import SlideTitle from './SlideTitle';
import SlideContent from './SlideContent';

export default class Slide extends Component {
  render() {
    return (
      <Paper className="slide" elevation={5}>
        <div className="divider">
          <span />
          <span>
            <SlideTitle />
          </span>
          <span></span>
        </div>
        <SlideContent />
      </Paper>
    )
  }
}