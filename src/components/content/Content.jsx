import React, { Component } from 'react';

import {
  Container,
  Paper
} from "@material-ui/core";

import Slide from './slide/Slide';
import SlideList from './slide/SlideList';

export default class Content extends Component {
  render() {
    return (
      <Container maxWidth="xl" className="content">
        <Paper className="slide-list-container" elevation={3}>
          <SlideList slidesList={[]} />
        </Paper>
        <Paper className="slide-container" elevation={3}>
          <Slide />
        </Paper>
      </Container>
    )
  }
}