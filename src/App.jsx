import React, { Component } from 'react';

import Header from './components/header/Header';
import Content from './components/content/Content';

import { Container } from '@material-ui/core';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isSignedIn: false
    }
  }

  authStateHandler(value) {
    this.setState({
      isSignedIn: value
    })
  }

  componentDidMount() {
    window.gapi.load('auth2', () => {
      window.gapi.auth2.init({
        client_id: '1003472031435-b3d56kfccak3sb6tefgmktds9d7a8uf0.apps.googleusercontent.com'
      })
    });
  }

  render() {
    return (
      <Container className="wrapper" maxWidth="xl" disableGutters={true}>
        <Header authStateHandler={this.authStateHandler.bind(this)}/>
        <Content />
      </Container>
    );
  }
}
